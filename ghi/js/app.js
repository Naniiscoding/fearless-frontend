function createCard(name, description, pictureUrl, start, end, location) {
    return `
      <div class="conference-card">
        <img src="${pictureUrl}" alt="Conference Picture">
        <h2>${name}</h2>
        <p>${description}</p>
        <p><strong>Date:</strong> ${start} - ${end}</p>
        <p><strong>Location:</strong> ${location}</p>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Handle response error if needed
        return;
      } else {
        const data = await response.json();
        const row = document.querySelector('.row'); // Select the row element

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toDateString();
            const end = new Date(details.conference.ends).toDateString();
            const location = details.conference.location.name;

            // Create the conference card HTML using the createCard function
            const html = createCard(name, description, pictureUrl, start, end, location);

            // Create a new column for each card
            const column = document.querySelector('.col');
            column.innerHTML += html;
            // Append the column to the row
            row.appendChild(column);
          }
        }
      }
    } catch (e) {
      console.error("Error:", e);
    }
  });
